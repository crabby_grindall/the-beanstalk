#ifndef PLAYER_H
#define PLAYER_H

#include "includes.h"
#include "Entity.h"
#include "Sprite.h"
#include <cmath>
#include <vector>

class Player: public Entity
{
	public:
		Player(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY);
		~Player();
		
		void getInput();
		void render();
		
		void applyWalking();
		void applyVelocity();
		
		void startJump();
		void endJump();
		
		int wallCollision(std::vector<SDL_Rect> rects);
		bool collisionX();
		bool collisionY();
		void setRects(std::vector<SDL_Rect> rects);
		SDL_Rect getRectp();
		int getLives();
		void harm();
		
		float getVelX();
		float getVelY();
		void setDead(bool p);
		
		bool restart();
		void setDoor(SDL_Rect a);
		bool DoorC();
		void next(bool a);
	private:
		Sprite* body;
		
		std::vector<SDL_Rect> cRects;
		SDL_Rect door;
		
		bool right;
		bool left;
		bool up;
		int animateState;
		int timeCheck;
		bool onGround;
		bool upReleased;
		bool doubleReady;
		int lives;
		bool dead;
		int timeSinceDead;
		bool gotDoor;
};

#endif
