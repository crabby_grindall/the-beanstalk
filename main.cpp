#include "GameManager.h"

int main()
{
	bool dead = false;
	while(!dead)
	{
		GameManager game;
		game.gameLoop();
		dead = !game.isDead();
	}
	return 0;
}
