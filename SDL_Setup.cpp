#include "SDL_Setup.h"

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

SDL_Setup::SDL_Setup()
{
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	
	window = NULL;
	
	mainEvent = new SDL_Event();
	window = SDL_CreateWindow("The Beanstalk: LUDUM DARE 34", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,SCREEN_WIDTH,SCREEN_HEIGHT,SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

 //Initialize SDL_mixer 
	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
	{
 		std::cout << "SDL_mixer could not initialize! SDL_mixer Error" << Mix_GetError() << std::endl;
	}
	Mix_Init(0);
}
SDL_Setup::~SDL_Setup()
{
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	
	SDL_Quit();
}
SDL_Event* SDL_Setup::getMainEvent()
{
	return mainEvent;
}
SDL_Renderer* SDL_Setup::getRenderer()
{
	return renderer;
}
void SDL_Setup::Poll( void )
{
	SDL_PollEvent( mainEvent );
}
void SDL_Setup::Begin( void )
{
	SDL_RenderClear( renderer );
}
void SDL_Setup::End( void )
{
	SDL_RenderPresent( renderer );
}
