#ifndef SOUND_H
#define SOUND_H

#include "includes.h"

class Sound
{
	public:
		Sound(std::string file);
		~Sound();
		void Play();
		void Halt();
	private:
		Mix_Chunk *gMusic;
};
#endif
