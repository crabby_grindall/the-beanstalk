#include "HeartUI.h"

HeartUI::HeartUI(SDL_Renderer* renderer, float* passed_cameraX, float* passed_cameraY)
{
	heart = NULL;
	heart = new Sprite(renderer, "data/graphics/heart-Sheet.png",24,24,24,24,passed_cameraX, passed_cameraY);
	heart->SetUpAnimation(1,3);
	
	heart1 = NULL;
	heart1 = new Sprite(renderer, "data/graphics/heart-Sheet.png",56,24,24,24,passed_cameraX, passed_cameraY);
	heart1->SetUpAnimation(1,3);
	
	heart2 = NULL;
	heart2 = new Sprite(renderer, "data/graphics/heart-Sheet.png",88,24,24,24,passed_cameraX, passed_cameraY);
	heart2->SetUpAnimation(1,3);
	lives = 3;
	x = 24;
	y = 24;
	firstTime = true;
}
void HeartUI::harmed()
{
	if(lives > 0 && SDL_GetTicks() - timer > 1000 && SDL_GetTicks() - immunity > 1000)
	{
		lives--;
		timer = SDL_GetTicks();
	}
}
void HeartUI::render()
{
	if(firstTime)
	{
		immunity = 0;
		immunity = SDL_GetTicks();
		lives = 3;
		firstTime = false;
	}
	if(lives == 0)
	{
		heart->PlayAnimation(2,2,0,0);
		heart1->PlayAnimation(2,2,0,0);
		heart2->PlayAnimation(2,2,0,0);
	}
	else if(lives == 1)
	{
		heart->PlayAnimation(0,0,0,0);
		heart1->PlayAnimation(2,2,0,0);
		heart2->PlayAnimation(2,2,0,0);
	}
	else if(lives == 2)
	{
		heart->PlayAnimation(0,0,0,0);
		heart1->PlayAnimation(0,0,0,0);
		heart2->PlayAnimation(2,2,0,0);
	}
	else if(lives == 3)
	{
		heart->PlayAnimation(0,0,0,0);
		heart1->PlayAnimation(0,0,0,0);
		heart2->PlayAnimation(0,0,0,0);
	}
	else
	{
		heart->PlayAnimation(2,2,0,0);
		heart1->PlayAnimation(2,2,0,0);
		heart2->PlayAnimation(2,2,0,0);	
	}

	heart->DrawPlayer();
	
	heart1->DrawPlayer();
	
	heart2->DrawPlayer();
	
}
bool HeartUI::isDead()
{
	if(lives==0)
	{
		return true;
	}
	return false;
}