#include "Tile.h"

Tile::Tile(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY, std::string file,int X, int Y, int sizeX, int sizeY, bool passed_solid, int tn, int tile_size)
{
	tile = NULL;
	
	tile = new Sprite(passed_setup->getRenderer(), file, X,Y,sizeX,sizeY, passed_CameraX, passed_CameraY);
	if(tile == NULL)
	{
		std::cout << "NULL TILES! WARNING!" << std::endl;
	}
	solid = passed_solid;
	door = false;
	tile->autoDetect(tile_size);
	//tile->SetUpAnimation(1,13);
	//tn/image_width
	if(tn != 1)
	{
		row = tn/tile->getFramesY();
		tile_x = tn % tile->getFramesY();
		tile_x -= 1;
	}
	else
	{
		tile_x = 0;
		row = 0;
	}
	if(tn == tile->getFramesY())
	{
		door = true;
		row = 0;
		tile_x = 13;
	}
}
Tile::~Tile()
{
	delete tile;
}
void Tile::setState(int state)
{
	switch(state)
	{
		case 0:
			//Do nothing
			break;
		case 1:
			solid = true;
			break;
		case 2:
			door = true;
			break;
	}
}

bool Tile::isSolid()
{
	return solid;
}
bool Tile::isDoor()
{
	return door;
}

SDL_Rect Tile::getRect()
{
	return tile->getRect();
}
void Tile::render()
{
	//First number is y 2nd is x
	tile->PlayAnimation(tile_x, tile_x, row, 0);
	tile->Draw();
}
