#ifndef TILEMANAGER_H
#define TILEMANAGER_H

#include "includes.h"
#include "Tile.h"

#include <cmath>
#include <vector>

class TileManager
{
	public:
		TileManager(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY);
		~TileManager();
		
		void initialize();
		
		void initRects();
		std::vector<SDL_Rect> getRects();
		void drawTiles();
		SDL_Rect getDoor();
		void clear();
	private:
		SDL_Setup* setup;
	
		float *CameraX;
		float *CameraY;
		
		std::vector<Tile*> tiles;
		SDL_Rect door;
		std::vector<SDL_Rect> solid_tiles;
		
		//width*height must equal levels size
		int width;
		int height;
		int level;
};

#endif
