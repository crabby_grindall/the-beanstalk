#ifndef SDL_SETUP_H
#define SDL_SETUP_H
#include "includes.h"

class SDL_Setup
{
	public:
		SDL_Setup();
		~SDL_Setup();
		SDL_Event* getMainEvent();
		SDL_Renderer* getRenderer();
		void Poll(void);
		void Begin(void);
		void End(void);
	private:
		SDL_Window* window;
		SDL_Renderer* renderer;
		SDL_Event* mainEvent;
};

#endif
