#include "Player.h"
#define MAX_SPEED 6

Player::Player(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY) : Entity(passed_setup, passed_CameraX, passed_CameraY)
{
	body = NULL;
	
	body = new Sprite(setup->getRenderer(), "data/graphics/Char_Sheet.png", 0,0,96,96, CameraX, CameraY);
	body->SetUpAnimation(4,12);
	
	right = true;
	left = false;
	up = false;
	onGround = false;
	animateState = 1;
	timeCheck = 0;
	doubleReady = false;
	upReleased = false;
	lives = 3;
	dead = false;
	timeSinceDead = 0;
};
Player::~Player()
{
	delete body;
	delete CameraX;
	delete CameraY;
}
void Player::getInput()
{
	switch(setup->getMainEvent()->type)
	{
		case SDL_KEYDOWN:
			switch (setup->getMainEvent()->key.keysym.sym)
			{
				case SDLK_w:
					//up
					startJump();
					up = true;
					upReleased = false;
					if(onGround)
					{
						animateState = 2;
					}
					break;
				/*case SDLK_a:
					//Left
					left = true;
					animateState = 0;
					break;
				case SDLK_d:
					//right
					right = true;
					animateState = 1;
					break;*/
				case SDLK_s:
					//right
					right = !right;
					left = !left;
					if(animateState == 0)
					{
						animateState = 1;
					}
					else if(animateState == 1)
					{
						animateState = 0;
					}
					break;

			}
			break;
		case SDL_KEYUP:
			switch (setup->getMainEvent()->key.keysym.sym)
			{
				case SDLK_w:
					//up
					endJump();
					if(!onGround)
					{
						upReleased = true;
					}
					up = false;
					break;
						
				/*case SDLK_a:
					//Left
					left = false;
					break;
							
				case SDLK_d:
					//right
					right = false;
					break;*/
			}
			break;
		}
}
void Player::applyWalking()
{
	if(!dead)
	{
		this->getInput();
	}
	if(right && velX < MAX_SPEED)
	{
		velX += 0.5;
	}
	if(left && velX > -MAX_SPEED)
	{
		velX -= 0.5;
	}
	if(up)
	{
		startJump();
	}
}
void Player::startJump()
{
	
	if(onGround)
	{
		velY = -12;
		onGround = false;
		up = false;
	}
	if(doubleReady && upReleased)
	{
		velY = -12;
		doubleReady = false;
	}
}
void Player::endJump()
{
	if(velY < -6)
	{
		velY = -6;
	}
}
void Player::applyVelocity()
{
	applyFriction();
	if(onGround)
	{
		if(velY > 0)
		{
			//velY = 0;
		}
	}
	else
	{
		if(velY < 10)
		{
			velY += gravity;
		}
	}
	if(!collisionX())
	{
		*CameraX -= velX;
	}
	if(!collisionY())
	{
		*CameraY -= velY;
		onGround = false;
	}

	body->setX(640/2 - cRect.w);
	body->setY(480/2 - cRect.h);
}
void Player::render()
{
	if(!onGround && !up)
	{
		body->PlayAnimation(8,8,animateState,0);
	}
	else if(velX != 0 && onGround && !up)
	{
		body->PlayAnimation(0,4,animateState,150);
	}
	else if(onGround && !up)
	{
		body->PlayAnimation(0,1,animateState + 2,300);
	}
	if(up && !dead)
	{
		body->PlayAnimation(5,11,animateState,250);
	}
	if(dead)
	{
		body->PlayAnimation(3,3,animateState + 2,0);
	}
	body->DrawPlayer();
}

bool Player::collisionX()
{
	for(std::vector<SDL_Rect>::iterator i = cRects.begin(); i < cRects.end();i++)
	{
		cRect = body->getRect();
		cRect.x -= *CameraX;
		cRect.y -= *CameraY;
		cRect.h -= 22;
		cRect.w -= 64;
		cRect.x += 32;
		cRect.x += velX;
		if(collision(cRect, door))
		{
			gotDoor = true;
		}
		if(collision(cRect, (*i)))
		{
			//onGround = false;
			return true;
		}
	}

	return false;
}
bool Player::collisionY()
{
	if(velY > 10)
	{
		velY = 10;
	}
	for(std::vector<SDL_Rect>::iterator i = cRects.begin(); i < cRects.end();i++)
	{
		cRect = body->getRect();
		cRect.x -= *CameraX;
		cRect.y -= *CameraY;
		cRect.h -= 22;
		cRect.w -= 64;
		cRect.x += 32;
		cRect.y += velY;
		if(collision(cRect, door))
		{
			gotDoor = true;
		}
		if(collision(cRect, (*i)))
		{
			onGround = false;
            if((cRect.y + cRect.h) - velY <= (*i).y)
			{
				onGround = true;
				doubleReady = true;
				upReleased = false;
			}
            return true;
		}
	}
	return false;
}

void Player::setRects(std::vector<SDL_Rect> rects)
{
	cRects = rects;
}
SDL_Rect Player::getRectp()
{
		cRect = body->getRect();
		cRect.x -= *CameraX;
		cRect.y -= *CameraY;
		cRect.h -= 22;
		cRect.w -= 64;
		cRect.x += 32;
		cRect.y += velY;
		cRect.x += velX;
		return cRect;
}
int Player::getLives()
{
	return lives;
}
void Player::harm()
{
	if(!dead)
	{
		body->PlayAnimation(2,2,animateState + 2,0);
	}
	if(lives > 0 && !dead)
	{
		lives--;
	}
}
float Player::getVelX()
{
	return velX;
}
float Player::getVelY()
{
	return velY;
}
void Player::setDead(bool p)
{

	if(*CameraY < -64*48)
	{
		p = true;
	}
	if(!dead && p)
	{
		timeSinceDead = SDL_GetTicks();
	}
	dead = p;
}
bool Player::restart()
{
	if(dead && SDL_GetTicks() - timeSinceDead > 3000)
	{
		return true;
	}
	return false;
}
void Player::setDoor(SDL_Rect a)
{
	door = a;
}
bool Player::DoorC()
{
	return gotDoor;
}
void Player::next(bool a)
{
	gotDoor = a;
	*CameraX = 0;
	*CameraY = 1000;
}