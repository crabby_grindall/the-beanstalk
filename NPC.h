#ifndef NPC_H
#define NPC_H

#include "Sprite.h"
#include "Entity.h"
#include "includes.h"
#include <vector>

class NPC : public Entity
{
	public:
		NPC(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY, std::string file, int x, int y,int size, int type);
		~NPC();
		
		void render();
		bool collisionX();
		bool collisionY();
		
		void startJump();
		void endJump();
		void setRects(std::vector<SDL_Rect> rects);
		
		void applyVelocity();
		void applyWalking();
		void addRect(SDL_Rect p);
		bool playerCollision();
		
		void setPVelX(float x);
		void setPVelY(float y);
		
		bool isAlive();
	private:
		Sprite* npc;
		
		int animateState;	
		bool onGround;
		
		std::vector<SDL_Rect> cRects;
		SDL_Rect prect;
		
		bool pColl;
		bool up;
		bool right;
		bool left;
		int type;
		bool alive;
		float pVelX;
		float pVelY;
		int timeSinceDeath;

};
#endif
