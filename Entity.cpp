#include "Entity.h"

Entity::Entity(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY)
{
	setup = passed_setup;
	CameraX = passed_CameraX;
	CameraY = passed_CameraY;
	
	velX = 0;
	velY = 0;
	
	friction = 0.2;
	gravity = 0.45;
}

Entity::~Entity()
{
	delete setup;
	
	delete CameraX;
	delete CameraY;
}

SDL_Rect Entity::getRect()
{
	return cRect;
}
void Entity::applyFriction()
{
	if(velX > 0)
	{
		velX -= friction;
	}
	else if(velX < 0)
	{
		velX += friction;
	}
	if((velX > -0.2 && velX < 0) || (velX < 0.2 && velX > 0))
	{
		velX = 0;
	}
}
void Entity::applyGravity()
{
	//Terminal velocity set to 10
	if(velY < 10)
	{
		velY += gravity;
	}
}
