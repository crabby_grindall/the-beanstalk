#ifndef SPRITE_H
#define SPRITE_H
#include"includes.h"
class Sprite
{
	public:
		Sprite(SDL_Renderer* passed_renderer, std::string FileName, int x, int y, int h, int w, float *passed_cameraX, float *passed_cameraY);
		~Sprite();
		
		float getX();
		float getY();
		float getWidth();
		float getHeight();
		void setX(float x);
		void setY(float y);
		SDL_Rect getRect();
		
		void PlayAnimation(int BeginFrame, int EndFrame, int Row, float Speed);
		void SetUpAnimation(int amountX, int amountY);
		void Draw();
		void DrawPlayer();
		
		void autoDetect(int tile_size);
		
		int getFramesX();
		int getFramesY();
		void ShowTile(int frame, int row);
	private:
		SDL_Rect Camera;
		float *CameraX;
		float *CameraY;
		
		SDL_Texture* image;
		SDL_Rect image_container;
		
		SDL_Rect cropped_image;
		SDL_Renderer *renderer;
		int image_width;
		int image_height;
		
		int CurrentFrame;
    	int animationDelay;
 
 	   int Amount_Frame_X;
 	   int Amount_Frame_Y;
};

#endif
