#include"includes.h"
#include "SDL_Setup.h"
#include "Sprite.h"
Sprite::Sprite(SDL_Renderer* passed_renderer, std::string FileName, int x, int y, int h, int w, float *passed_cameraX, float *passed_cameraY)
{
    CameraX = passed_cameraX;
    CameraY = passed_cameraY;
	renderer = passed_renderer;
    image = NULL;
	image = IMG_LoadTexture( renderer, FileName.c_str());

	image_container.x = x;
    image_container.y = y;
    image_container.w = w;
    image_container.h = h;
	
    Camera.x = *CameraX;
    Camera.y = *CameraY;
    Camera.w = image_container.w;
    Camera.h = image_container.h;
    
	SDL_QueryTexture(image,NULL,NULL, &image_width, &image_height);
 
    cropped_image.x = 0;
    cropped_image.y = 0;
    cropped_image.w = image_width;
    cropped_image.h = image_height;
    animationDelay = 0;
    if(image == NULL)
    {
        std::cout << "NULL TEXTURE!" << std::endl;
    }
}
Sprite::~Sprite()
{
	SDL_DestroyTexture( image );
	SDL_DestroyRenderer( renderer );

	delete CameraX;
	delete CameraY;
}
void Sprite::Draw()
{
	Camera.x = image_container.x + *CameraX;
	Camera.y = image_container.y + *CameraY;
	
	SDL_RenderCopy(renderer,image, &cropped_image, &Camera);
}

void Sprite::DrawPlayer()
{
	SDL_RenderCopy(renderer,image, &cropped_image, &image_container);
}

float Sprite::getX()
{
	return image_container.x;
}
float Sprite::getY()
{
	return image_container.y;
}
float Sprite::getWidth()
{
    return image_container.w;
}
float Sprite::getHeight()
{
    return image_container.h;
}
void Sprite::setX(float x)
{
	   image_container.x = x;
}
void Sprite::setY(float y)
{
        image_container.y = y;
}
void Sprite::PlayAnimation(int BeginFrame, int EndFrame, int Row, float Speed)
{
    if (animationDelay+Speed < SDL_GetTicks())
    {
 
    if (EndFrame <= CurrentFrame)
        CurrentFrame = BeginFrame;
    else
        CurrentFrame++;
 
    cropped_image.x = CurrentFrame * (image_height/Amount_Frame_X);
    cropped_image.y = Row * (image_width/Amount_Frame_Y);
    cropped_image.w = image_height/Amount_Frame_X;
    cropped_image.h = image_width/Amount_Frame_Y;
 
    animationDelay = SDL_GetTicks();
    }
}
void Sprite::ShowTile(int frame, int row)
{
    cropped_image.x = frame * (image_height/Amount_Frame_Y);
    cropped_image.y = row * (image_width/Amount_Frame_X);
    cropped_image.w = image_height/Amount_Frame_Y;
    cropped_image.h = image_width/Amount_Frame_X;
}
void Sprite::SetUpAnimation(int amountX, int amountY)
{
    Amount_Frame_X = amountX;
    Amount_Frame_Y = amountY;
}
SDL_Rect Sprite::getRect()
{
    return image_container;
}
void Sprite::autoDetect(int tile_size)
{
    int x = image_width/tile_size;
    int y = image_height/tile_size;
    
    Amount_Frame_X = y;
    Amount_Frame_Y = x;
}
int Sprite::getFramesX()
{
    return Amount_Frame_X;
}
int Sprite::getFramesY()
{
    return Amount_Frame_Y;
}
