#ifndef ENTITY_H
#define ENTITY_H
#include "includes.h"
#include "SDL_Setup.h"
#include "collision/collision.cpp"

class Entity
{
	public:
		Entity(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY);
		~Entity();
		SDL_Rect getRect();
		float posX;
		float posY;
		float velX;
		float velY;
		SDL_Rect cRect;
		
		float *CameraX;
		float *CameraY;
		
		float gravity;
		float friction;
		
		SDL_Setup* setup;
		void applyFriction();
		void applyGravity();
};
#endif
