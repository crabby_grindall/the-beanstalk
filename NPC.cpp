#include "NPC.h"

NPC::NPC(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY, std::string file, int x, int y,int size, int type) : Entity(passed_setup, passed_CameraX, passed_CameraY)
{
	npc = NULL;
	
	if(type == 0)
	{
		npc = new Sprite(passed_setup->getRenderer(),file,x,y,size,size, passed_CameraX, passed_CameraY );
		npc->SetUpAnimation(2,6);
		animateState = 0;
		up = false;
		right = true;
		left = false;
		onGround = false;
	}
	else if(type == 1)
	{
		npc = new Sprite(passed_setup->getRenderer(),file,x,y,size,size, passed_CameraX, passed_CameraY );
		npc->SetUpAnimation(1,5);
		animateState = 0;
		up = true;
		right = false;
		left = false;
		onGround = false;
	}
	velX = 0;
	velY = 0;
	if(npc == NULL)
	{
		std::cout << "NULL NPC" << std::endl;
	}
	posX = x;
	posY = y;
	this->type = type;
	pVelX = 0;
	pVelY = 0;
	alive = true;
	timeSinceDeath = 0;
}
NPC::~NPC()
{
	delete npc;
}

void NPC::render()
{
	//up = true;
	if(type == 0)
	{
		if(velX < 0 && alive)
		{
			animateState = 0;
		}
		else if(velX > 0 && alive)
		{
			animateState = 1;
		}
		if(velX != 0 && alive)
		{
			npc->PlayAnimation(0,4,animateState,150);
		}
		else if(!alive)
		{
			npc->PlayAnimation(5,5,animateState,300);
		}
	}
	else if(type == 1 && alive)
	{
		npc->PlayAnimation(0,3,0,300);
	}
	else if(type == 1 && !alive)
	{
		npc->PlayAnimation(4,4,0,300);
	}
	npc->Draw();
}
bool NPC::collisionX()
{
	for(std::vector<SDL_Rect>::iterator i = cRects.begin(); i < cRects.end();i++)
	{
		cRect = npc->getRect();
		cRect.h -= 26;
		cRect.x += velX;
		cRect.y -= velY;
		if(collision(cRect, (*i)))
		{
			//onGround = false;
			if(type == 0)
			{
				right = !right;
				left = !left;
				velX = -velX;
			}
			return true;
		}
	}
		if(collision(cRect, prect))
		{
			if(type == 0)
			{
				right = !right;
				left = !left;
				velX = -velX;
				pColl = true;
			}
			if((prect.y + prect.h) - pVelY <= cRect.y)
			{
				pColl = false;
				if(timeSinceDeath == 0)
				{
					timeSinceDeath = SDL_GetTicks();
				}
				npc->PlayAnimation(4,4,0,300);
				up = false;
				left = false;
				right = false;
				velX = 0;
				velY = 0;
				alive = false;
			}
            return true;
		}
	return false;
}
bool NPC::collisionY()
{
	int c;
	if(velY > 10)
	{
		velY = 10;
	}
	for(std::vector<SDL_Rect>::iterator i = cRects.begin(); i < cRects.end();i++)
	{
		cRect = npc->getRect();
		cRect.h -= 22;
		cRect.y += velY;
		cRect.x -= velX;
		if(collision(cRect, (*i)))
		{
			onGround = false;	
            if((cRect.y + cRect.h) - velY <= (*i).y)
			{
				onGround = true;
			}
			if(type == 0)
			{
            	return true;
			}
			c = 1;
		}
	}
	if(collision(cRect, prect))
	{
		onGround = false;	
        if((cRect.y + cRect.h) - velY <= prect.y)
		{
			pColl = true;
			onGround = true;
		}
		if((prect.y + prect.h) - pVelY <= cRect.y)
		{
			npc->PlayAnimation(4,4,0,300);
			pColl = false;
			alive = false;
			up = false;
			left = false;
			right = false;
			velX = 0;
			velY = 0;
			if(timeSinceDeath == 0)
			{
				timeSinceDeath = SDL_GetTicks();
			}
		}
        return true;
	}
	if(c == 1)
	{
		return true;
	}
	return false;
}
void NPC::startJump()
{
	if(onGround)
	{
		velY = -12;
		onGround = false;
		up = false;
	}
}
void NPC::endJump()
{
	if(velY < -6)
	{
		velY = -6;
	}
}
void NPC::setRects(std::vector<SDL_Rect> rects)
{
	cRects = rects;
}
void NPC::applyVelocity()
{
	applyFriction();
	if(onGround)
	{
	}
	else
	{
		if(velY < 10)
		{
			velY += gravity;
		}
	}
	if(!collisionX() && alive)
	{
		posX += velX;
	}
	if(!collisionY() && alive)
	{
		//if(type == 0)
		//{
			posY += velY;
			onGround = false;	
		//}
	}
	npc->setX(posX);
	npc->setY(posY);
}
void NPC::applyWalking()
{
	if(type == 1 && alive)
	{
		up = true;	
	}
	//right = true;
	if(right && velX < 2)
	{
		velX += 0.5;
	}
	if(left && velX > -2)
	{
		velX -= 0.5;
	}
	if(up)
	{
		startJump();
	}
}
void NPC::addRect(SDL_Rect p)
{
	prect = p;	
}
bool NPC::playerCollision()
{
	bool r;
	if(pColl)
	{
		pColl = false;
		r = true;
	}
	if(!alive)
	{
		r = false;
	}
	return r;
}
void NPC::setPVelX(float x)
{
	pVelX = x;
}
void NPC::setPVelY(float y)
{
	pVelY = y;
}
bool NPC::isAlive()
{
	if(!alive && SDL_GetTicks() - timeSinceDeath > 1000)
	{
		return false;
	}
	return true;
}