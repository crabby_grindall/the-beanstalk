#include "Title.h"

Title::Title(SDL_Renderer* renderer, float* passed_CameraX, float* passed_CameraY)
{
	title = NULL;
	button = NULL;
	
	title = new Sprite(renderer, "data/graphics/Title_Screen.png",0,0,480,640,passed_CameraX, passed_CameraY);
	button = new Sprite(renderer, "data/graphics/play_button.png",320- 100,240,64,128,passed_CameraX, passed_CameraY);
}
Title::~Title()
{
	delete title;
	delete button;
}

void Title::getMouse()
{
	SDL_GetMouseState(&mX,&mY);
}
bool Title::checkPressed()
{
	SDL_Rect b = button->getRect();
	SDL_Rect a;
	a.x = mX;
	a.y = mY;
	a.w = 0;
	a.h = 0;
	if(a.x < b.x + b.w && a.x + a.w > b.x && a.y < b.y + b.h && a.y + a.h > b.y )
	{
		if(SDL_BUTTON(1))
		{
			return true; 
		}
	}
	return false;
}
void Title::render()
{
	title->DrawPlayer();
	button->DrawPlayer();
}
