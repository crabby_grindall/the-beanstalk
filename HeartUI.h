#ifndef HEARTUI_H
#define HEARTUI_H

#include "includes.h"
#include "Sprite.h"

class HeartUI
{
	public:
		HeartUI(SDL_Renderer* renderer, float* passed_cameraX, float* passed_cameraY);
		
		void harmed();
		bool isDead();
		void render();
	private:
		Sprite* heart;
		Sprite* heart1;
		Sprite* heart2;
		int lives;
		int x;
		int y;
		int timer;
		int immunity;
		bool firstTime;
};

#endif
