#include "TileManager.h"

TileManager::TileManager(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY)
{
	setup = passed_setup;
	
	CameraX = passed_CameraX;
	CameraY = passed_CameraY;

	width = 16;
	height = 16;
	level = 0;
}
TileManager::~TileManager()
{
	delete setup;
	
	delete CameraX;
	delete CameraY;
}
void TileManager::initialize()
{
	//Layer 1
	if(level == 0)
	{
		int layer2[] = {
			#include "data/Level_Arrays/level0.txt"
		};
		int n = sizeof(layer2)/sizeof(layer2[0]);
		height = sqrt(n);
		width = height;
		int count = 0;
		//Tileset
		std::string file2 = "data/graphics/grass-Sheet.png";
		for(int k = 0; k < height; k++)
		{
			for(int l = 0; l < width; l++)
			{
				if(layer2[count] != 0)
				{
					tiles.push_back(new Tile(setup, CameraX,CameraY, file2,l*48,k*48,48,48, true,layer2[count],16));
				}
				count++;
			}
		}
	}
	else if(level == 1)
	{
		int layer2[] = {
			#include "data/Level_Arrays/level1.txt"
		};
		int n = sizeof(layer2)/sizeof(layer2[0]);
		height = sqrt(n);
		width = height;
		int count = 0;
		//Tileset
		std::string file2 = "data/graphics/grass-Sheet.png";
		for(int k = 0; k < height; k++)
		{
			for(int l = 0; l < width; l++)
			{
				if(layer2[count] != 0)
				{
					tiles.push_back(new Tile(setup, CameraX,CameraY, file2,l*48,k*48,48,48, true,layer2[count],16));
				}
				count++;
			}
		}
	}
}

void TileManager::initRects()
{
	for(std::vector<Tile*>::iterator i = tiles.begin(); i < tiles.end();i++)
	{
		if((*i)->isSolid())
		{
			solid_tiles.push_back((*i)->getRect());
		}
	}
}

std::vector<SDL_Rect> TileManager::getRects()
{
	return solid_tiles;
}

void TileManager::drawTiles()
{
	for(std::vector<Tile*>::iterator i = tiles.begin(); i < tiles.end();i++)
	{
		(*i)->render();	
	}	
}
SDL_Rect TileManager::getDoor()
{
	SDL_Rect a;
	for(std::vector<Tile*>::iterator i = tiles.begin(); i < tiles.end();i++)
	{
		if((*i)->isDoor())
		{
			a = (*i)->getRect();
		}
	}	
	return a;
}
void TileManager::clear()
{
	level++;
	while(!tiles.empty())
	{
		tiles.pop_back();
	}
	while(!solid_tiles.empty())
	{
		solid_tiles.pop_back();
	}
}