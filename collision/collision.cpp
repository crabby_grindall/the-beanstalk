#ifndef COLLISION_CPP
#define COLLISION_CPP

#include "../includes.h"

bool static collision(SDL_Rect a, SDL_Rect b)
{
	if(a.x < b.x + b.w && a.x + a.w > b.x && a.y < b.y + b.h && a.y + a.h > b.y)
	{
		return true;
	}
	return false;
	
}

#endif
