#ifndef NPCMANAGER_H
#define NPCMANAGER_H

#include "includes.h"
#include "NPC.h"
#include <vector>

class NpcManager
{
	public:
		NpcManager(SDL_Setup* passed_setup, float* passed_cameraX, float* passed_cameraY);
	
		void addNPC(int type,int x, int y);
		void updateNpc();
		void drawNpc();
		void checkDead();
		void updateInfo(float p, std::vector<SDL_Rect> r, SDL_Rect k);
		bool pHarm();
		void init();
		void clear();
	private:
		SDL_Setup* setup;
		
		float* CameraX;
		float* CameraY;
		
		std::vector<NPC*> npcs;
		
		std::vector<SDL_Rect> rects;
		SDL_Rect prect;
		float pvely;
		int level;
};
#endif
