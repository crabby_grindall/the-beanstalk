#ifndef TILE_H
#define TILE_H

#include "includes.h"

#include "Sprite.h"
#include "SDL_Setup.h"

class Tile
{
	public:
		Tile(SDL_Setup* passed_setup, float *passed_CameraX, float *passed_CameraY, std::string file, int X, int Y, int sizeX, int sizeY, bool passed_solid, int tn,int tile_size);
		~Tile();
		
		void setState(int state);
		
		bool isSolid();
		bool isDoor();
		SDL_Rect getRect();
		
		void render();
	private:
	
		Sprite* tile;
		
		float posX;
		float posY;
		
		int tile_x;
		int row;
		
		bool door;
		bool solid;	
};

#endif
