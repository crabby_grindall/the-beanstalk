#include "GameManager.h"

GameManager::GameManager()
{
	CameraX = 0;
	CameraY = 0;
	
	renderer = NULL;
	setup = NULL;
	background = NULL;
	tiles = NULL;
	player = NULL;
	hearts = NULL;
	nMan = NULL;
	title = NULL;
	setup = new SDL_Setup();
	renderer = setup->getRenderer();
	title = new Title(renderer,&CameraX,&CameraY);
	sd = new Sound("NOTHIGN");
	player = new Player(setup, &CameraX, &CameraY);
	//sd->Play();
	tiles = new TileManager(setup,&CameraX, &CameraY);
	tiles->initialize();
	tiles->initRects();
	background = new Sprite(setup->getRenderer(), "data/graphics/bg-Sheet.png",0,0,480,640,&CameraX, &CameraY);
	background->SetUpAnimation(1,16);
	if(background == NULL)
	{
		std::cout << "NULL BACKGROUND! Retrying..." << std::endl;
		background = new Sprite(setup->getRenderer(), "data/graphics/bg-Sheet.png",0,0,480,640,&CameraX, &CameraY);
		background->SetUpAnimation(1,16);
	}
	hearts = new HeartUI(setup->getRenderer(), &CameraX, &CameraY);
	nMan = new NpcManager(setup,&CameraX, &CameraY);
	nMan->init();
	titleScreen = true;
	dead = false;
}
GameManager::~GameManager()
{
	SDL_DestroyRenderer(renderer);
	delete setup;
	//delete background;
	//delete npc;
	//delete tiles;
	//delete player;
}
void GameManager::gameLoop()
{

	Timer fpsTimer;
	Timer capTimer;
	std::stringstream timeText;
	int countedFrames = 0;
	fpsTimer.start();
	
	while(setup->getMainEvent()->type != SDL_QUIT && !dead)
	{
 		//Start cap timer 
		capTimer.start();
 		//Calculate and correct fps 
		float avgFPS = countedFrames / ( fpsTimer.getTicks() / 1000.f ); 
		if( avgFPS > 2000000 ) 
		{ 
			 avgFPS = 0; 
		} 
		setup->Begin();
		//Polling main event
		setup->Poll();
		if(!titleScreen)
		{
			if(player->DoorC())
			{
				std::cout << "BANANAS!" << std::endl;
				tiles->clear();
				nMan->clear();
				tiles->initialize();
				tiles->initRects();
				player->setRects(tiles->getRects());
				nMan->updateInfo(player->getVelY(),tiles->getRects(),player->getRectp());
				nMan->init();
				player->next(false);
			}
			nMan->checkDead();
			//Main code her
			player->setRects(tiles->getRects());
			player->setDoor(tiles->getDoor());
			player->applyWalking();
			player->applyVelocity();
			
			nMan->updateInfo(player->getVelY(),tiles->getRects(),player->getRectp());
			nMan->updateNpc();
			if(nMan->pHarm())
			{
				player->harm();
				hearts->harmed();
			}
			player->setDead(hearts->isDead());
			background->PlayAnimation(0,15,0,400);
			background->DrawPlayer();
			player->render();
			nMan->drawNpc();
			tiles->drawTiles();
			hearts->render();
			if(player->restart())
			{
				dead = true;
			}

		}
		else
		{
			title->getMouse();
			if(title->checkPressed() && setup->getMainEvent()->type == SDL_MOUSEBUTTONUP)
			{
				titleScreen = false;
			}
			title->render();
		}
		setup->End();
		++countedFrames;
		int frameTicks = capTimer.getTicks(); 
		if( frameTicks < SCREEN_TICKS_PER_FRAME ) 
		{ 
			SDL_Delay( SCREEN_TICKS_PER_FRAME - frameTicks ); 
		}
	}
}
bool GameManager::isDead()
{
	return dead;
}