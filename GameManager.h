#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#include "includes.h"
#include "SDL_Setup.h"
#include "Sprite.h"
#include "Sound.h"
#include "Timer.h"
#include "Player.h"
#include "TileManager.h"
#include "NPC.h"
#include "HeartUI.h"
#include "NpcManager.h"
#include "Title.h"

const int SCREEN_WIDTH = 640; 
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;
const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;

class GameManager
{
	public:
		GameManager();
		~GameManager();
		void gameLoop();
		bool isDead();
	private:
		SDL_Setup* setup;
		SDL_Renderer* renderer;
		Sound* sd;
		Sprite* background;
		HeartUI* hearts;
		Player* player;
		TileManager* tiles;
		Title* title;
		NpcManager* nMan;
		float CameraX;
		float CameraY;
		bool titleScreen;
		bool dead;
};
#endif
