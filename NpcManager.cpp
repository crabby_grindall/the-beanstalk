#include "NpcManager.h"
NpcManager::NpcManager(SDL_Setup* passed_setup, float* passed_cameraX, float* passed_cameraY)
{
	setup = NULL;
	CameraX = NULL;
	CameraY = NULL;
	
	setup = passed_setup;
	CameraX = passed_cameraX;
	CameraY = passed_cameraY;
	level = 0;
}
void NpcManager::addNPC(int type,int x, int y)
{
	std::string file;
	if(type == 0)
	{
		file = "data/graphics/Turtle-Sheet.png";
	}
	else if(type = 1)
	{
		file = "data/graphics/Chicken-Sheet.png";
	}
	npcs.push_back(new NPC(setup, CameraX, CameraY, file, x,y,48,type));
}
void NpcManager::updateNpc()
{
	for(std::vector<NPC*>::iterator i = npcs.begin(); i != npcs.end();i++)
	{
		(*i)->setPVelY(pvely);
		(*i)->setRects(rects);
		(*i)->addRect(prect);
		(*i)->applyWalking();
		(*i)->applyVelocity();
	}
}
void NpcManager::drawNpc()
{
	for(std::vector<NPC*>::iterator i = npcs.begin(); i != npcs.end();i++)
	{
		(*i)->render();
	}
}
void NpcManager::checkDead()
{
	for(std::vector<NPC*>::iterator i = npcs.begin(); i < npcs.end();i++)
	{
		if(!(*i)->isAlive())
		{
			npcs.erase(i);
		}
	}
}
void NpcManager::updateInfo(float p, std::vector<SDL_Rect> r, SDL_Rect k)
{
	pvely = p;
	rects = r;
	prect = k;
}
bool NpcManager::pHarm()
{
	bool p = false;
	for(std::vector<NPC*>::iterator i = npcs.begin(); i != npcs.end();i++)
	{
		if((*i)->playerCollision())
		{
			p = true;
		}
	}
	return p;
}
void NpcManager::init()
{
	if(level == 0)
	{
		int height = 0;
		int width = 0;
		int layer2[] = {
			#include "data/Level_Arrays/level0e.txt"
	};
		int n = sizeof(layer2)/sizeof(layer2[0]);
		height = sqrt(n);
		width = height;
		int count = 0;
		//Tileset
		for(int k = 0; k < height; k++)
		{
			for(int l = 0; l < width; l++)
			{
				if(layer2[count] != 0)
				{
					addNPC(layer2[count]-1,l*48,k*48);
				}
				count++;
			}
		}
	}
	if(level == 1)
	{
		int height = 0;
		int width = 0;
		int layer2[] = {
			#include "data/Level_Arrays/level1e.txt"
	};
		int n = sizeof(layer2)/sizeof(layer2[0]);
		height = sqrt(n);
		width = height;
		int count = 0;
		//Tileset
		for(int k = 0; k < height; k++)
		{
			for(int l = 0; l < width; l++)
			{
				if(layer2[count] != 0)
				{
					addNPC(layer2[count]-1,l*48,k*48);
				}
				count++;
			}
		}
	}
}
void NpcManager::clear()
{
	while(!npcs.empty())
	{
		npcs.pop_back();
	}
	level++;
}