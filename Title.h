#ifndef TITLE_H
#define TITLE_H

#include "includes.h"
#include "Sprite.h"

class Title
{
	public:
		Title(SDL_Renderer* renderer, float* passed_CameraX, float* passed_CameraY);
		~Title();
		
		void getMouse();
		bool checkPressed();
		
		void render();
	private:
		Sprite* title;
		Sprite* button;
	
		int mX;
		int mY;
		bool playPressed;
		
};
#endif
